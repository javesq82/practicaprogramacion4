package practicaprogramacion4;

class Cajero {
    private int fondosCuenta;

    public Cajero(int fondosCuenta) {
        this.fondosCuenta = fondosCuenta;
    }

    public int getFondosCuenta() {
        return fondosCuenta;
    }

    public boolean depositar(int deposito) {
        if (deposito > 0) {
            fondosCuenta = fondosCuenta + deposito;
            return true;
        }
        return false;
    }

    public boolean retirar(int retiro) {
        if (fondosCuenta >= retiro && retiro > 0) {
            fondosCuenta = fondosCuenta - retiro;
            return true;
        }
        return false;
    }

}